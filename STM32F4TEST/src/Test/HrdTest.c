/*
 * HrdTest.c
 *
 *  Created on: 27 ��. 2017 �.
 *      Author: bogdan.kostiv
 */

#include <stdio.h>
#include <string.h>
#include "HrdTest.h"

#ifdef USE_SWO
#include "SWO/swo.h"
#endif

#ifdef USE_RTT
#include "RTT/SEGGER_RTT.h"
#endif

typedef enum {
	DBG_MSG_START = 1,
	DBG_MSG_EVENT,
	DBG_MSG_STOP
} DbgMsgType_t;

volatile static uint8_t HrdTest_Dbg(DbgMsgType_t msg, bool res);
#ifdef USE_SWO
static int8_t SWO_Debug(char *message);
#endif

volatile uint8_t msg_st = 0;
volatile uint8_t res_st = 0;

uint8_t HrdTest_Dbg(DbgMsgType_t msg, bool res)
{
	volatile static uint8_t dmp_st;

	msg_st = msg;
	res_st = res;

	dmp_st = 0;
	return 0;
}

void HrdTest_Init()
{
#ifdef USE_RTT
	SEGGER_RTT_ConfigUpBuffer(0, NULL, NULL, 0, SEGGER_RTT_MODE_NO_BLOCK_SKIP);
	SEGGER_RTT_WriteString(0, "SEGGER Real-Time-Terminal Sample\r\n\r\n");
	SEGGER_RTT_printf(0, "START\r\n");
#endif

#ifdef USE_SWO
	SWO_Init();
	SWO_Print("SEGGER SWO Sample\r\n\r\n");
	SWO_Print("START\r\n");
#endif

	HrdTest_Dbg(DBG_MSG_START, true);
}

void HrdTest_TestResult(bool res, char *message, uint8_t length)
{
	char resultBuff[128];
	message[length]='\0';
	if (res)
	{
		sprintf(resultBuff, "PASSED: %s\r\n", message);
		HrdTest_Dbg(DBG_MSG_EVENT, true);
	}
	else
	{
		sprintf(resultBuff, "FAILED: %s\r\n", message);
		HrdTest_Dbg(DBG_MSG_EVENT, false);
	}

#ifdef USE_RTT
	SEGGER_RTT_WriteString(0, resultBuff);
#endif
#ifdef USE_SWO
	SWO_Print(resultBuff);
#endif
}

void HrdTest_Close()
{
#ifdef USE_RTT
	SEGGER_RTT_printf(0, "STOP\r\n");
#endif
#ifdef USE_SWO
	SWO_Print("STOP\r\n");
#endif
	HrdTest_Dbg(DBG_MSG_STOP, true);
}
