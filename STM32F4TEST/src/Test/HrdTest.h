/*
 * RTTTest.h
 *
 *  Created on: 27 ��. 2017 �.
 *      Author: bogdan.kostiv
 */

#ifndef TEST_HRDTEST_H_
#define TEST_HRDTEST_H_

#include <stdbool.h>

#define USE_RTT
//#define USE_SWO

void HrdTest_Init();

void HrdTest_TestResult(bool res, char *message, uint8_t length);

void HrdTest_Close();

#endif /* TEST_HRDTEST_H_ */
